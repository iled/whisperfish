<!--
If you found a bug in our Rust rework, you are of course free to report an
issue.  Many of these regressions however are already documented on the issue
tracker https://gitlab.com/rubdos/whisperfish/-/issues, so please search
thoroughly before posting, or ask on Matrix #whisperfish:rubdos.be or Freenode
#whisperfish for some help.
-->

## Summary

<!-- summarize the regression: what was the expected behaviour, what did 0.5
do, what does our version do. -->

## Trace log

<!--
Please run harbour-whisperfish from the command-line as follows:

  $ RUST_LOG=trace harbour-whisperfish 2> /tmp/whisperfish-log.txt

Reproduce the issue, and include the file in `/tmp/whisperfish-log.txt`
-->

```
paste trace log here <-
```
